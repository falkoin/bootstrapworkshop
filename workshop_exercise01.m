%% load data
load( 'exercise01.mat' );
handoverStart = 1;
meanHandoverEnd = length( data.displacement{1} );
color.receiver = [1, 0.75, 0.3];
color.passer = [0.3, 0.75, 0.9];
color.blind = [0.1, 0.7, 0.1];
color.all = [0.1, 0.1, 0.1];
color.both = [0.1, 0.1, 0.8];
color.glove = [1, 0.65, 0.3];

condition.is.medium = 'medium';
condition.is.glove = 'glove';
condition.is.all = 'all';
condition.is.blind = 'blind';
condition.is.both = 'blind with glove';

%% split by medium and blind
% creates logical vectors for conditions
dataIsMedium = strcmp( data.Speed, condition.is.medium ); 
dataIsGlove = strcmp( data.Sensory, condition.is.glove );
dataIsAll = strcmp( data.Sensory, condition.is.all );
dataIsBlind = strcmp( data.Sensory, condition.is.blind);
dataIsBoth = strcmp( data.Sensory, condition.is.both ); 

% combines conditions
dataMediumGlove = data(dataIsGlove & dataIsMedium, :);
dataMediumAll = data(dataIsAll & dataIsMedium, :);
dataMediumBlind = data(dataIsBlind & dataIsMedium, :);
dataMediumBoth = data(dataIsBoth & dataIsMedium, :);


% find matching samples and create means over subjects
tableGroups = findgroups( dataMediumGlove.Subject );
meanCurvesDisplacementMediumGlove = splitapply( @mean, cell2mat( dataMediumGlove.displacement ), tableGroups );
tableGroups = findgroups( dataMediumAll.Subject );
meanCurvesDisplacementMediumAll = splitapply( @mean, cell2mat( dataMediumAll.displacement ), tableGroups );
tableGroups = findgroups( dataMediumBlind.Subject );
meanCurvesDisplacementMediumBlind = splitapply( @mean, cell2mat( dataMediumBlind.displacement ), tableGroups );
tableGroups = findgroups( dataMediumBoth.Subject );
meanCurvesDisplacementMediumBoth = splitapply( @mean, cell2mat( dataMediumBoth.displacement ), tableGroups );

%% PBRT
% create difference curve

% PBRT parameters
effectWindowStart = handoverStart;
effectWindowEnd = meanHandoverEnd;
% nSamples = 400;
confidenceBandWidth = 0.95;

% PBRT
cSaturation = 5;
nSamples = pbrtCalculateOptimalSampleSize( meanCurvesDisplacementMediumAll(:, effectWindowStart : effectWindowEnd)', cSaturation );
[meanCurveAll, upperBandAll, lowerBandAll] = pbrtCreateBands( meanCurvesDisplacementMediumAll(:, effectWindowStart : effectWindowEnd)', nSamples, confidenceBandWidth );
[meanCurveGlove, upperBandGlove, lowerBandGlove] = pbrtCreateBands( meanCurvesDisplacementMediumGlove(:, effectWindowStart : effectWindowEnd)', nSamples, confidenceBandWidth );
[meanCurveBlind, upperBandBlind, lowerBandBlind] = pbrtCreateBands( meanCurvesDisplacementMediumBlind(:, effectWindowStart : effectWindowEnd)', nSamples, confidenceBandWidth );
[meanCurveBoth, upperBandBoth, lowerBandBoth] = pbrtCreateBands( meanCurvesDisplacementMediumBoth(:, effectWindowStart : effectWindowEnd)', nSamples, confidenceBandWidth );

%% plots
t = linspace( 0, 100, length( meanCurvesDisplacementMediumGlove ) );
figure()
% all mean curves per subject and condition
subplot( 2, 2, 1 )
hold on
title( 'Mean curves', 'FontSize', 48 )
plot( t, meanCurvesDisplacementMediumGlove', 'color', color.all, 'LineWidth', 3)
plot( t, meanCurvesDisplacementMediumAll', 'color', color.glove, 'LineWidth', 3)
plot( t, meanCurvesDisplacementMediumBoth', 'color', color.both, 'LineWidth', 3)
plot( t, meanCurvesDisplacementMediumBlind', 'color', color.blind, 'LineWidth', 3)
axis tight
ylim( [0, 6] )
xlabel( 'Relative handover duration (percentage)', 'FontSize', 32 )
ylabel( 'Displacement (cm)', 'FontSize', 32 )

% grand average per condition
subplot( 2, 2, 2 )
hold on
title( 'Grand average curves', 'FontSize', 48 )
plot( t, mean( meanCurvesDisplacementMediumGlove ), 'color', color.all, 'LineWidth', 3)
plot( t, mean( meanCurvesDisplacementMediumAll ), 'color', color.glove, 'LineWidth', 3)
plot( t, mean( meanCurvesDisplacementMediumBoth ), 'color', color.both, 'LineWidth', 3)
plot( t, mean( meanCurvesDisplacementMediumBlind ), 'color', color.blind, 'LineWidth', 3)
axis tight
legend( 'All', 'Glove', 'Blind', 'Blind with glove', 'FontSize', 32 )
ylabel( 'Displacement (cm)', 'FontSize', 32 )
ylim( [0, 6] )
xlabel( 'Relative handover duration (percentage)', 'FontSize', 32 )

% Confidencebands
subplot( 2, 2, [3, 4] )
hold on
title( 'PBRT bands with grand average curves', 'FontSize', 48 )
bandAlpha = 0.5;
errorBand( t(:, effectWindowStart : effectWindowEnd), meanCurveAll, meanCurveAll-lowerBandAll, 3, color.all, color.all );
errorBand( t(:, effectWindowStart : effectWindowEnd), meanCurveGlove, meanCurveGlove-lowerBandGlove, 3, color.glove, color.glove );
errorBand( t(:, effectWindowStart : effectWindowEnd), meanCurveBlind, meanCurveBlind-lowerBandBlind, 3, color.blind, color.blind );
errorBand( t(:, effectWindowStart : effectWindowEnd), meanCurveBoth, meanCurveBoth-lowerBandBoth, 3, color.both, color.both );
xlim( [t(1), t(end)] )
ylabel( 'Displacement (cm)', 'FontSize', 32 )
xlabel( 'Relative handover duration (percentage)', 'FontSize', 32 )