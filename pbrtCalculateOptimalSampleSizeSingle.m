function nSamples = pbrtCalculateOptimalSampleSizeSingle(data, goalVarianceC, varargin)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

if isempty( varargin ) == 1
  confidenceLevel = 0.95;
else
  if varargin{1} > 1
    confidenceLevel = varargin{1}/100;
  else
    confidenceLevel = varargin{1};
  end
end
nSamples = 5;
simulationRuns = 100;
cVariance = 100;
cOfSimulations = nan( 1, simulationRuns );

while cVariance > goalVarianceC
  fprintf( 'Current number of samples is: %i\n', nSamples );
  fprintf( 'Simulating %i samples\n', simulationRuns );
  for currentRun = 1 : simulationRuns
    fprintf( '.' );
    lengthCurves = size( data, 1 );
    nDataCurves = size( data, 2 );
    
    bootstrapSamples = zeros( nDataCurves, lengthCurves );
    grandAverage = zeros( nSamples, lengthCurves );
    differences = zeros( nSamples, lengthCurves );
    
    for i = 1: nSamples
      for k = 1 : nDataCurves
        bootstrapSamples(k,:) = data(:, randi( nDataCurves ) );
      end
      grandAverage(i,:) = mean( bootstrapSamples );
      standardError = std( grandAverage );
      
      
      for curveIdx = 1: nDataCurves
        differenceRaw = grandAverage(i, :) - bootstrapSamples(curveIdx,:);
        quotient = differenceRaw ./ standardError;
        differences(i, curveIdx) = max( abs( quotient ) );
      end
    end
    factorC = sort( differences(:) );
    cutIdxForC = round( length( factorC ) * confidenceLevel );
    factorC = factorC(cutIdxForC);
    cOfSimulations( currentRun ) = factorC;
  end
  meanC = mean( cOfSimulations );
  stdC = std( cOfSimulations );
  cVariance = (stdC/meanC) * 100;
  fprintf( '\nCurrent relative C variance is: %.2f %%\n', cVariance );
  
  nSamples = nSamples * 2;
end

% upperBand = grandAveragePopulation + factorC * standardError;
% lowerBand = grandAveragePopulation - factorC * standardError;
%
% grandAverage = grandAveragePopulation;
end