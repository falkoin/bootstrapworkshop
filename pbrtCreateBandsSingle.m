function [grandAverage, upperBand, lowerBand, factorC] = pbrtCreateBandsSingle(data, nSamples, varargin)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

if isempty( varargin ) == 1
  confidenceLevel = 0.95;
else
  if varargin{1} > 1
    confidenceLevel = varargin{1}/100;
  else
    confidenceLevel = varargin{1};
  end
end

lengthCurves = size( data, 1 );
nDataCurves = size( data, 2 );

bootstrapSamples = zeros( nDataCurves, lengthCurves );
grandAverage = zeros( nSamples, lengthCurves );
differences = zeros( nSamples, lengthCurves );

for i = 1 : nSamples
  for k = 1 : nDataCurves
    bootstrapSamples(k,:) = data(:, randi( nDataCurves ) );
  end
  grandAverage(i,:) = mean( bootstrapSamples );
  standardError = std( bootstrapSamples );
  
  for curveIdx = 1 : nDataCurves
    differenceRaw = grandAverage(i, :) - bootstrapSamples(curveIdx,:);
    quotient = differenceRaw ./ standardError;
    differences(i,curveIdx) = max( abs( quotient ) );
  end
end

grandAveragePopulation = mean( grandAverage );
factorC = sort( differences(:) );
cutIdxForC = round( length( factorC ) * confidenceLevel );
factorC = factorC(cutIdxForC);

upperBand = grandAveragePopulation + factorC * standardError;
lowerBand = grandAveragePopulation - factorC * standardError;

grandAverage = grandAveragePopulation;
end