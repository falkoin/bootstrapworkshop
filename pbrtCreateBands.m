function [grandAverage, upperBand, lowerBand, factorC] = pbrtCreateBands(data, nSamples, varargin)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

if isempty( varargin ) == 1
  confidenceLevel = 0.95;
else
  if varargin{1} > 1
    confidenceLevel = varargin{1}/100;
  else
    confidenceLevel = varargin{1};
  end
end

lengthCurves = size( data, 1 );
nDataCurves = size( data, 2 );

bootstrapSamples = zeros( nDataCurves, lengthCurves );
grandAverage = zeros( nSamples, lengthCurves );

for i = 1: nSamples
  for k = 1 : nDataCurves
    bootstrapSamples(k,:) = data(:, randi( nDataCurves ) );
  end
  grandAverage(i,:) = mean( bootstrapSamples );
end

grandAveragePopulation = mean( grandAverage );
standardError = std( grandAverage );

differences = zeros( nSamples );

for i = 1: nSamples
  differenceRaw = grandAverage(i, :) - grandAveragePopulation;
  quotient = differenceRaw / standardError;
  
%   quotient = np.divide(differenceRaw, standardError, out=np.zeros_like(differenceRaw), where=standardError != 0)
  differences(i) = max( abs( quotient ) );
end

factorC = sort( differences );
cutIdxForC = round( length( factorC ) * confidenceLevel );
factorC = factorC(cutIdxForC);

upperBand = grandAveragePopulation + factorC * standardError;
lowerBand = grandAveragePopulation - factorC * standardError;

grandAverage = grandAveragePopulation;
end


%
% def bootstrapFull(data, nSamples, confidenceLevel):
%     lengthCurves = len(data[0])
%     # print(len(data[0]))
%     nDataCurves = len(data)
%
%     bootstrapSamples = np.zeros((nDataCurves, lengthCurves))
%     grandAverage = np.zeros((nSamples, lengthCurves))
%
%     for i in range(0, nSamples):
%         for k in range(0, nDataCurves):
%             bootstrapSamples[k] = data[np.random.randint(0, nDataCurves)]
%
%         grandAverage[i] = np.mean(bootstrapSamples, axis=0)
%
%     grandAveragePopulation = np.mean(grandAverage, axis=0)
%     standardError = np.std(grandAverage, axis=0)
%
%     differences = np.zeros(nSamples)
%
%     for i in range(0, nSamples):
%         differenceRaw = grandAverage[i, ] - grandAveragePopulation
%         quotient = np.divide(differenceRaw, standardError, out=np.zeros_like(differenceRaw), where=standardError != 0)
%         differences[i] = np.max(np.abs(quotient))
%
%     factorC = np.sort(differences)
%     cutIdxForC = int(np.round(len(factorC) * confidenceLevel))
%     factorC = factorC[cutIdxForC]
%
%     upperBand = grandAveragePopulation + factorC * standardError
%     lowerBand = grandAveragePopulation - factorC * standardError
%
%     return grandAveragePopulation, upperBand, lowerBand




%
% #!/usr/bin/env python3
% # -*- coding: utf-8 -*-
% """
% Created on Mon May  6 08:58:59 2019
%
% @author: phrk
% """
% import numpy as np
%
%
% def create_bands(df, data_identifier, group_identifier, n_bootstrap_samples, confidence_level=0.95):
%
%     if confidence_level > 1:
%         confidence_level = confidence_level / 100
%
%     data = []
%     for sbj, df in df.groupby(group_identifier):
%         data.append(df[data_identifier].tolist())
%     data = np.array(data)
%
%     n_data_curves = len(data)
%     print("Number of Curves: " + str(n_data_curves))
%     length_data_curves = len(data[0])
%     print("Number of Data Points: " + str(length_data_curves))
%
%     bootstrap_samples = np.zeros((n_data_curves, length_data_curves))
%     grand_average = np.zeros((n_bootstrap_samples, length_data_curves))
%
%     for sample_idx in range(0, n_bootstrap_samples):
%         for curve_idx in range(0, n_data_curves):
%             random_sample = np.random.randint(0, n_data_curves)
%             print(random_sample)
%             bootstrap_samples[curve_idx] = data[random_sample]
%
%         grand_average[sample_idx] = np.mean(bootstrap_samples, axis=0)
%
%     grand_average_population = np.mean(grand_average, axis=0)
%     standard_error = np.std(grand_average, axis=0)
%
%     differences = np.zeros(n_bootstrap_samples)
%
%     for sample in range(0, n_bootstrap_samples):
%         differences[sample] = np.max(np.abs((grand_average[sample, ] - grand_average_population) / standard_error))
%
%     c_list = np.sort(differences)
%     cut_index_for_c = int(np.round(len(c_list) * confidence_level))
%     factor_c = c_list[cut_index_for_c]
%
%     upper_band = grand_average_population + factor_c * standard_error
%     lower_band = grand_average_population - factor_c * standard_error
%
%     return grand_average_population, upper_band, lower_band
