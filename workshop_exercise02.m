%% load data
load( 'exercise02.mat' );
handoverStart = 509;
meanHandoverEnd = 1390;
color.receiver = [1, 0.75, 0.3];
color.passer = [0.3, 0.75, 0.9];
color.all = [0.1, 0.1, 0.1];
color.glove = [1, 0.65, 0.3];

condition.is.medium = 'medium';
condition.is.glove = 'glove';
condition.is.all = 'all';

%% split by medium and blind
% creates logical vectors for conditions
dataIsMedium = strcmp( data.Speed, condition.is.medium ); 
dataIsGlove = strcmp( data.Sensory, condition.is.glove );
dataIsAll = strcmp( data.Sensory, condition.is.all );

% combines conditions
dataMediumGlove = data(dataIsGlove & dataIsMedium, :);
dataMediumAll = data(dataIsAll & dataIsMedium, :);

% find matching samples and create means over subjects
tableGroups = findgroups( dataMediumGlove.Subject );
meanCurvesPasserMediumGlove = splitapply( @mean, cell2mat( dataMediumGlove.passer ), tableGroups );
tableGroups = findgroups( dataMediumAll.Subject );
meanCurvesPasserMediumAll = splitapply( @mean, cell2mat( dataMediumAll.passer ), tableGroups );

%% PBRT
% create difference curve

differenceCurves = meanCurvesPasserMediumGlove - meanCurvesPasserMediumAll;
% PBRT parameters
lengthPredictivePhase = 200;
effectWindowStart = handoverStart + lengthPredictivePhase;
effectWindowEnd = meanHandoverEnd;
% nSamples = 400;
confidenceBandWidth = 0.95;

% PBRT
cSaturation = 5;
nSamples = pbrtCalculateOptimalSampleSize( differenceCurves(:, effectWindowStart : effectWindowEnd)', cSaturation );
[meanCurve, upperBand, lowerBand] = pbrtCreateBands( differenceCurves(:, effectWindowStart : effectWindowEnd)', nSamples, confidenceBandWidth );
zeroCurve = zeros( 1, length( meanCurve ) );
[acceptH1, nCrosses] = pbrtCheckHypothesis( zeroCurve, upperBand, lowerBand);

%% plots
t = linspace( -handoverStart/1000, (length( meanCurvesPasserMediumGlove )/1000) - handoverStart/1000, length( meanCurvesPasserMediumGlove ) );
figure()
% all mean curves per subject and condition
subplot( 2, 2, 1 )
hold on
title( 'Mean curves', 'FontSize', 48 )
plot( t, meanCurvesPasserMediumGlove', 'color', color.glove, 'LineWidth', 3)
plot( t, meanCurvesPasserMediumAll', 'color', color.all, 'LineWidth', 3)
axis tight
ylim( [0, 80] )
xlabel( 'time (s)', 'FontSize', 32 )
ylabel( 'force (N)', 'FontSize', 32 )

% grand average per condition
subplot( 2, 2, 2 )
hold on
title( 'Grand average curves', 'FontSize', 48 )
plot( t, mean( meanCurvesPasserMediumGlove ), 'color', color.glove, 'LineWidth', 3)
plot( t, mean( meanCurvesPasserMediumAll ), 'color', color.all, 'LineWidth', 3)
axis tight
legend( 'Glove', 'All', 'FontSize', 32 )
ylabel( 'force (N)', 'FontSize', 32 )
ylim( [0, 80] )
xlabel( 'time (s)', 'FontSize', 32 )

% differencecurve
subplot( 2, 2, 3 )
hold on
plot( t, mean( differenceCurves ), 'r', 'LineWidth', 3 ) 
title( 'Difference curves', 'FontSize', 48 )
patch( [t(effectWindowStart), t(effectWindowStart), t(effectWindowEnd), t(effectWindowEnd)], [0, 20, 20, 0], [0, 1, 0], 'EdgeColor', 'none' )
axis tight
textHeight = 19;
text( t(effectWindowEnd), textHeight, 'Effect window', 'FontSize', 32 )
alpha(0.5)
ylim( [0, 20] );
ylabel( 'force (N)', 'FontSize', 32 )
xlabel( 'time (s)', 'FontSize', 32 )

% Confidencebands
subplot( 2, 2, 4 )
hold on
title( 'PBRT difference curves with bands', 'FontSize', 48 )
errorBand( t(:, effectWindowStart : effectWindowEnd), meanCurve, meanCurve-lowerBand, 3, [0, 0, 0, 0] );
hold on
plot( t(:, effectWindowStart : effectWindowEnd), zeroCurve , 'g--', 'LineWidth', 3 )
xlim( [t(1), t(end)] )
ylabel( 'force (N)', 'FontSize', 32 )
xlabel( 'time (s)', 'FontSize', 32 )