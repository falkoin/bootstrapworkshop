function nSamples = pbrtCalculateOptimalSampleSize(data, goalVarianceC, varargin)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

if isempty( varargin ) == 1
  confidenceLevel = 0.95;
else
  if varargin{1} > 1
    confidenceLevel = varargin{1}/100;
  else
    confidenceLevel = varargin{1};
  end
end
nSamples = 5;
simulationRuns = 100;
cVariance = 100;
cOfSimulations = nan( 1, simulationRuns );

while cVariance > goalVarianceC
  fprintf( 'Current number of samples is: %i\n', nSamples ); 
  fprintf( 'Simulating %i samples\n', simulationRuns ); 
  for currentRun = 1 : simulationRuns
    fprintf( '.' ); 
    lengthCurves = size( data, 1 );
    nDataCurves = size( data, 2 );

    bootstrapSamples = zeros( nDataCurves, lengthCurves );
    grandAverage = zeros( nSamples, lengthCurves );

    for i = 1: nSamples
      for k = 1 : nDataCurves
        bootstrapSamples(k,:) = data(:, randi( nDataCurves ) );
      end
      grandAverage(i,:) = mean( bootstrapSamples );
    end

    grandAveragePopulation = mean( grandAverage );
    standardError = std( grandAverage );

    differences = zeros( nSamples );

    for i = 1: nSamples
      differenceRaw = grandAverage(i, :) - grandAveragePopulation;
      quotient = differenceRaw / standardError;

      %   quotient = np.divide(differenceRaw, standardError, out=np.zeros_like(differenceRaw), where=standardError != 0)
      differences(i) = max( abs( quotient ) );
    end

    factorC = sort( differences );
    cutIdxForC = round( length( factorC ) * confidenceLevel );
    factorC = factorC(cutIdxForC);
    cOfSimulations( currentRun ) = factorC;
  end
  meanC = mean( cOfSimulations );
  stdC = std( cOfSimulations );
  cVariance = (stdC/meanC) * 100;
  fprintf( '\nCurrent relative C variance is: %.2f %%\n', cVariance ); 
  
  nSamples = nSamples * 2;
end

% upperBand = grandAveragePopulation + factorC * standardError;
% lowerBand = grandAveragePopulation - factorC * standardError;
%
% grandAverage = grandAveragePopulation;
end