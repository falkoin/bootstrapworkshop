close all

%% prepare data
dataPasser = readtable( 'workshop_data_passer.csv', 'ReadRowNames', false );
dataReceiver = readtable( 'workshop_data_receiver.csv', 'ReadRowNames', false );

%% restructure data
dataCellPasser = cell( height( dataPasser )-1, 1 );
dataCellReceiver = cell( height( dataPasser )-1, 1 );
for tableRow = 2 : height( dataPasser )
  dataCellPasser{tableRow-1, 1} = table2array( dataPasser(tableRow, 2:end-3) );
  dataCellReceiver{tableRow-1, 1} = table2array( dataReceiver(tableRow, 2:end-3) );
end

%% Exercise 2
dataPasser(:, 2:end-3) = [];
dataReceiver(:, 2:end-3) = [];
dataPasser(1, :) = [];
dataReceiver(1, :) = [];
dataPasser.Properties.VariableNames = {'Curve', 'Subject', 'Speed', 'Sensory'};
dataPasser.passer = dataCellPasser;
dataPasser.receiver = dataCellReceiver;
data = dataPasser;

dataIsMedium = strcmp( data.Speed, 'medium' );
dataIsMedium = logical( abs( dataIsMedium-1 ) ); % switch 0 to 1

data( dataIsMedium, : ) = []; % remove all but medium

save( 'exercise02.mat', 'data' );

%% prepare data 2

dataDisplacement = readtable( 'workshop_data_displacement.csv', 'ReadRowNames', false );

%% restructure data
dataCellDisplacement = cell( height( dataDisplacement )-1, 1 );
for tableRow = 2 : height( dataDisplacement )
  dataCellDisplacement{tableRow-1, 1} = table2array( dataDisplacement(tableRow, 2:end-3) );
end

%% Exercise 2
dataDisplacement(:, 2:end-3) = [];
dataDisplacement(1, :) = [];
dataDisplacement.Properties.VariableNames = {'Curve', 'Subject', 'Speed', 'Sensory'};
dataDisplacement.displacement = dataCellDisplacement;
data = dataDisplacement;

dataIsMedium = strcmp( data.Speed, 'medium' );
dataIsMedium = logical( abs( dataIsMedium-1 ) ); % switch 0 to 1

data( dataIsMedium, : ) = []; % remove all but medium

save( 'exercise01.mat', 'data' );

%% prepare data 3

dataVelocity = readtable( 'workshop_data_velocity.csv', 'ReadRowNames', false );

%% restructure data
dataCellVelocity = cell( height( dataVelocity )-1, 1 );
for tableRow = 2 : height( dataVelocity )
  dataCellVelocity{tableRow-1, 1} = table2array( dataVelocity(tableRow, 2:end-3) );
end

%% Exercise 2
dataVelocity(:, 2:end-3) = [];
dataVelocity(1, :) = [];
dataVelocity.Properties.VariableNames = {'Curve', 'Subject', 'Speed', 'Sensory'};
dataVelocity.velocity = dataCellVelocity;
data = dataVelocity;

% dataIsMedium = strcmp( data.Speed, 'medium' );
% dataIsMedium = logical( abs( dataIsMedium-1 ) ); % switch 0 to 1

data.Sensory = []; % remove column sensory
data.Subject = []; % remove column subject

save( 'exercise03.mat', 'data' );