function [acceptH1, nCrosses] = pbrtCheckHypothesis(h0Curve, upperBand, lowerBand)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
acceptH1 = any( h0Curve > upperBand) | any( h0Curve < lowerBand );
nCrosses.abs = sum( h0Curve > upperBand | h0Curve < lowerBand  );
nCrosses.relative = (nCrosses.abs / length( upperBand ) ) * 100;
end

