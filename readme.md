
![](logo_readme.jpg)
# Bootstrap Workshop 18.12.2020
## Datensatz
### Eckdaten
Kraft- und Positionsdaten einer Übergabe-Studie

![](fig01.png)
<figcaption><b>Abb. 1</b> <i>Übergabeobjekt, bestehend aus 4 gepaarten Kraftsensoren (links). Typischer Kraftverlauf einer Übergabe (rechts).</i></figcaption>

- Aufnahmerate 1000 Hz
- Geschnitten auf 2106 Datenpunkte
- Synchronisiert auf den Start (Datenpunkt 509)

### Studiendesign
- 4 Reichgeschwindigkeiten des Receivers
	- Slow, **Medium**, Fast, Very Fast
- 4 Sensorische Bedingungen
	- **All**, **Blind**, **Glove**, **Both**


## Anwendung 1 - Abgrenzung unterschiedlicher Experimentbedingungen

### Untersuchungsgegenstand
Wurde das Ziel unterschiedliche Reichgeschwindigkeiten des Übernehmers erreicht?

### Datensatz
Höhenveränderung des Übergabeobjekts während der Übergabe

![](fig02.png)
<figcaption><b>Abb. 2</b> <i>Tabellarische Darstellung des Datensatzes "Höhenveränderung" in MATLAB</i></figcaption>

### Arbeitsschritte

- Aufteilung der Daten nach Bedingungen
- Erstellung mittlerer Kurven pro Bedingung
- Festlegen des "effect window"
- Berechnung der Anzahl benötigter Stichproben für das "resampling"
- PBRT
- Abbildungen

[MATLAB Datei 1](workshop_exercise01.m)

## Anwendung 2 - Abhängige Daten
### Untersuchungsgegenstand
Gibt es einen Unterschied zwischen den Bedingungen 'all' und 'glove' nach der Prediktionsphase?

### Datensatz
Kraftdaten des Übergebers
Mittleres Ende der Übergabe: 1390

![](fig03.png)
<figcaption><b>Abb. 3</b> <i>Tabellarische Darstellung des Datensatzes "Kraftdaten" in MATLAB</i></figcaption>

### Arbeitsschritte

- Aufteilung der Daten nach Bedingungen
- Erstellung mittlerer Kurven pro Bedingung
- Differenzbildung zwischen den Bedingungen
- Festlegen des "effect window"
- Berechnung der Anzahl benötigter Stichproben für das "resampling"
- PBRT
- Abbildungen

[MATLAB Datei 2](workshop_exercise02.m)

## Anwendung 3 - Testung einzelner Kurven auf Zugehörigkeit
### Untersuchungsgegenstand
Können schnellen Reichbewegungen von moderaten differenziert werden?
### Datensatz
Geschwindigkeitsdaten des Receivers


![](fig04.png)
<figcaption><b>Abb. 4</b> <i>Tabellarische Darstellung des Datensatzes "Greifgeschwindigkeit" in MATLAB</i></figcaption>

### Arbeitsschritte

- Aufteilung der Daten nach Bedingungen
- Festlegen des "effect window"
- Berechnung der Anzahl benötigter Stichproben für das "resampling"
- PBRT
- Berechnung der abweichenden Kurven
- Abbildungen

[MATLAB Datei 3](workshop_exercise03.m)