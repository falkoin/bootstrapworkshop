%% load data
load( 'exercise03.mat' );
handoverStart = 1;
meanHandoverEnd = length( data.velocity{1} );
style.fast = 'k';
style.medium = 'g';

condition.is.fast = 'fast';
condition.is.medium = 'medium';

%% split by medium and blind
% creates logical vectors for conditions
dataIsMedium = strcmp( data.Speed, condition.is.medium ); 
dataIsFast = strcmp( data.Speed, condition.is.fast );

% combines conditions
dataFast = data(dataIsFast, :);
dataMedium = data(dataIsMedium, :);

dataCurvesFast = cell2mat( dataFast.velocity );
dataCurvesMedium = cell2mat( dataMedium.velocity );

%% PBRT
% PBRT parameters
effectWindowStart = handoverStart+50;
effectWindowEnd = round( meanHandoverEnd*0.5 );
% nSamples = 1000;
confidenceBandWidth = 0.95;

% PBRT
cSaturation = 5;
nSamples = pbrtCalculateOptimalSampleSizeSingle( dataCurvesMedium(:, effectWindowStart : effectWindowEnd)', cSaturation );
[meanCurve, upperBand, lowerBand] = pbrtCreateBandsSingle( dataCurvesMedium(:, effectWindowStart : effectWindowEnd)', nSamples, confidenceBandWidth );
curvesOutsideInPercentage = sum(any(dataCurvesFast(:, effectWindowStart : effectWindowEnd) > upperBand, 2) | any(dataCurvesFast(:, effectWindowStart : effectWindowEnd) < lowerBand, 2)) / size(dataCurvesFast, 1) * 100;
%% plots
dataFrequency = 1000;
t = linspace( 0, (length( dataCurvesMedium )/dataFrequency), length( dataCurvesMedium ) );
figure()
% all mean curves per subject and condition
subplot( 2, 1, 1 )
hold on
title( 'All curves', 'FontSize', 48 )
patch( [t(effectWindowStart), t(effectWindowStart), t(effectWindowEnd), t(effectWindowEnd)], [0, 20, 20, 0], [0.7, 0.7, 0.7], 'EdgeColor', 'none' )
plot( t, dataCurvesFast', style.fast, 'LineWidth', 3)
plot( t, dataCurvesMedium', style.medium, 'LineWidth', 3)

axis tight
textHeight = 19;
text( t(effectWindowEnd), textHeight, 'Effect window', 'FontSize', 32 )
alpha(0.5)
ylim( [0, 1.6] )
xlabel( 'time (s)', 'FontSize', 32 )
ylabel( 'reaching velocity (m/s)', 'FontSize', 32 )

% Confidencebands
subplot( 2, 1, 2 )
hold on
title( 'PBRT bands with fast curves', 'FontSize', 48 )
hold on
plot( t(:, effectWindowStart : effectWindowEnd), dataCurvesFast(:, effectWindowStart : effectWindowEnd) , 'g-', 'LineWidth', 2 )
plot( t(:, effectWindowStart : effectWindowEnd), upperBand , 'k--', 'LineWidth', 3 )
plot( t(:, effectWindowStart : effectWindowEnd), lowerBand , 'k--', 'LineWidth', 3 )

xlim( [t(1), t(end)] )
ylabel( 'reaching velocity (m/s)', 'FontSize', 32 )
xlabel( 'time (s)', 'FontSize', 32 )
